# rocker-geospatial-rNOMADS

This image is based on [rocker/geospatial](https://hub.docker.com/r/rocker/geospatial/), a Docker-based Geospatial toolkit for R, built on versioned Rocker images. It adds support to [wgrib2](http://www.cpc.ncep.noaa.gov/products/wesley/wgrib2/) and [wgrib](http://www.cpc.ncep.noaa.gov/products/wesley/wgrib.html) for [rNOMADS](https://cran.r-project.org/web/packages/rNOMADS/index.html).

# Usage

Check out [this page](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image).

# Set wgrib and wgrib2 in the path variable from R

If RStudio cannot find `wgrib` and `wgrib2` with the `system()` command, you can set the path from R by running the following at the start of a script:

```
path = Sys.getenv("PATH")
new_path <- paste0(path, ":/usr/local/wgrib2:/usr/local/wgrib")
Sys.setenv(PATH = new_path)

```